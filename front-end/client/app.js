import jQuery from 'jquery';
import $ from 'jquery';
import Main from './js/main';

class Root {
    constructor() {
        this.main_page = new Main();
    }
}


window.$ = jQuery;

var App = new Root();
window.App = App;