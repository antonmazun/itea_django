запуск проекта:

1) git clone https://gitlab.com/antonmazun/itea_django.git

2) создаем виртуальное окружение с консоли для бек-енда

2.1) открываем в консоли проект в директории где лежит папка eshop , front-end , etc.

2.2) делаем виртуальное окружение командой -- python -m venv env 

2.3) затем активируем его -- cd env/Scripts/ нажимаем Enter , затем вводим команду -- activate


2.4) заходим в папку eshop

2.5) устанавливаем все зависимости которые есть в проекте  - pip install -r req.txt

2.6) после того как установили зависимости заходим в следующую папку eshop 

2.7) Делаем миграции python manage.py migrate

2.8) запускаем сервер python manage.py runserver

Красаучики!) 




3) Собираем front-end webpack-ом. Если нет вебпака -> https://webpack.js.org/guides/installation/
3.1) Открываем консоль там где лежит package.json

3.2) Делаем npm install 

3.3) После того как установили все фронт-енд зависимости выполняем команду npm run watch 
