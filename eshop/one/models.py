from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
#
# class Post(models.Model):
#     title = models.CharField(max_length=120)
#     category = TreeForeignKey('Category', null=True, blank=True , on_delete=models.CASCADE)
#     content = models.TextField('Content')
#     slug = models.SlugField()
#
#     def __str__(self):
#         return self.title
#
#
# class Category(MPTTModel):
#     name = models.CharField(max_length=50, unique=True)
#     parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True , on_delete=models.CASCADE)
#     slug = models.SlugField()
#
#     class MPTTMeta:
#         order_insertion_by = ['name']
#
#     class Meta:
#         unique_together = (('parent', 'slug',))
#         verbose_name_plural = 'categories'
#
#     def get_slug_list(self):
#         try:
#             ancestors = self.get_ancestors(include_self=True)
#         except:
#             ancestors = []
#         else:
#             ancestors = [i.slug for i in ancestors]
#         slugs = []
#         for i in range(len(ancestors)):
#             slugs.append('/'.join(ancestors[:i + 1]))
#         return slugs
#
#     def __str__(self):
#         return self.name


from django.db import models
from pytils.translit import slugify


class Example(models.Model):
    integer_field = models.IntegerField()
    positive_field = models.PositiveIntegerField()
    positive_small_field = models.PositiveSmallIntegerField()
    big_integer_field = models.BigIntegerField()
    float_field = models.FloatField()
    binary_field = models.BinaryField()
    boolean_field = models.BooleanField()
    char_field = models.CharField(max_length=5)
    text_field = models.TextField(max_length=20)
    date_field = models.DateField(auto_now=False)
    date_time_field = models.DateTimeField(auto_now_add=False)
    decimal_field = models.DecimalField(max_digits=8, decimal_places=2)  # 222222.22
    email = models.EmailField()
    file_field = models.FileField(upload_to='file')
    image_field = models.ImageField(upload_to='images')

    def __str__(self):
        return 'Pos_field is {}'.format(self.positive_field)


class Author(models.Model):
    name = models.CharField(max_length=50, verbose_name="Имя")
    surname = models.CharField(max_length=50, verbose_name="Фамилия")
    date_birth = models.DateField(auto_now=False, verbose_name="Дата рождения")

    def __str__(self):
        return "Имя : %s Фамилия : %s" % (self.name, self.surname)

    class Meta:
        verbose_name = "Автор"
        verbose_name_plural = "Авторы"


class Book(models.Model):
    CHOISE_GENRE = (
        ('comedy', "Comedy"),
        ('tragedy', "Tragedy"),
        ('drama', "Drama"),
    )

    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    text = models.TextField(max_length=1000)
    genre = models.CharField(max_length=50, choices=CHOISE_GENRE)

    def __str__(self):
        return self.title + ' ' + self.author.name + ' ' + self.text


class Place(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=80)

    def __str__(self):  # __unicode__ on Python 2
        return "%s the place" % self.name


class Restaurant(models.Model):
    place = models.OneToOneField(Place, on_delete=models.CASCADE, primary_key=True)
    serves_hot_dogs = models.BooleanField(default=False)
    serves_pizza = models.BooleanField(default=False)

    def __str__(self):  # __unicode__ on Python 2
        return "%s the restaurant" % self.place.name


class Waiter(models.Model):
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    def __str__(self):  # __unicode__ on Python 2
        return "%s the waiter at %s" % (self.name, self.restaurant)


class Publication(models.Model):
    title = models.CharField(max_length=30)

    def __str__(self):  # __unicode__ on Python 2
        return self.title

    class Meta:
        ordering = ('title',)


class Article(models.Model):
    headline = models.CharField(max_length=100)
    publications = models.ManyToManyField(Publication)

    def __str__(self):  # __unicode__ on Python 2
        return self.headline

    class Meta:
        ordering = ('headline',)


class Category(models.Model):
    title = models.CharField(max_length=255, default='')
    slug = models.SlugField(unique=True, blank=False)

    def save(self, commit=False, *args, **kwargs):
        self.slug = slugify(self.title)
        print(self.slug)
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class BaseProduct(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=None)
    year = models.DateField()
    brand = models.CharField(max_length=255, verbose_name='Бренд')
    model = models.CharField(max_length=255, verbose_name="Модель")
    price = models.FloatField(verbose_name='Цена')


class Laptop(BaseProduct):
    TYPE_MONITOR = (
        ('m', 'Матовый'),
        ('g', 'Глянцевый')
    )

    count_core = models.IntegerField(verbose_name='Кол-во ядер')
    proc = models.CharField(max_length=255, verbose_name='Процессор')
    type_monitor = models.CharField(max_length=255, choices=TYPE_MONITOR)


    def __str__(self):
        return '{} {}$'.format(self.brand , self.price)
