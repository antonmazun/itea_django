from django.shortcuts import render, get_object_or_404
from .models import *
from django.http import HttpResponse

from action_user.models import Photo

# Create your views here.
#
# def post_list(request, slug=None):
#     posts = Post.objects.all()
#     categories  = Category.objects.all()
#     print(categories)
#     return render(request, "one/main.html", {'posts': posts})
#
#
# def post_detail(request, slug=None):
#     instance = get_object_or_404(Post, slug=slug)
#     return render(request, "one/post_detail.html", {'instance': instance})
#
#
# def show_category(request,hierarchy= None):
#     category_slug = hierarchy.split('/')
#     parent = None
#     root = Category.objects.all()
#     for slug in category_slug[:-1]:
#         parent = root.get(parent=parent, slug = slug)
#     try:
#         instance = Category.objects.get(parent=parent,slug=category_slug[-1])
#     except:
#         instance = get_object_or_404(Post, slug = category_slug[-1])
#         return render(request, "one/post_detail.html", {'instance':instance})
#     else:
#         return render(request, 'one/categories.html', {'instance':instance})
#
# ctx = {}
# ctx['all_categories'] = Category.objects.all()
#
# def index(request):
#     loca_ctx = {}
#     return render(request , 'one/main.html' , ctx)
#
#
global_ctx = {
    'all_categories': Category.objects.all()
}


def category_items(request, slug):
    local_ctx = {}
    category = Category.objects.get(slug=slug)
    all_products_in_cat = BaseProduct.objects.filter(category=category)
    local_ctx['all_products_in_cat'] = all_products_in_cat
    global_ctx.update(local_ctx)
    if all_products_in_cat:
        print('cool!')
    else:
        print('not cool!')
    return render(request, 'one/all_products_in_cat.html', global_ctx)


from .calc import calc_obj


def index(request):
    if request.method == 'GET':
        print(request.META['HTTP_USER_AGENT'])
        local_ctx = {}
        all_places = Place.objects.all()
        local_ctx['a'] = 5
        local_ctx['b'] = 5
        # local_ctx['all_places'] = all_places
        local_ctx['all_operation'] = calc_obj.keys()
        # local_ctx['photos'] = Photo.objects.all()
        global_ctx.update(local_ctx)
        #at once
        # request.session.clear()
        if not request.session.get('backet'):
            backet = {
                'user_id': request.user.id if request.user.id else 'anonym',
                'backet_list': []
            }
            request.session['backet'] = backet
        return render(request, 'one/main.html', global_ctx)
    elif request.method == 'POST':
        local_ctx = {}
        first_number = float(request.POST.get('first_number'))
        second_number = float(request.POST.get('second_number'))
        operation = request.POST.get('operation')
        res = None
        if operation:
            res = calc_obj[operation](first_number, second_number)
        local_ctx['res'] = res
        global_ctx.update(local_ctx)
        return render(request, 'one/main.html', global_ctx)



def test_form(request):
    if request.method == 'POST':
        req_post  = request.POST
        first_name = req_post.get('first_name')
        surname = req_post.get('surname')
        gender = req_post.get('gender')
        check = req_post.get('check')
        gender_1 = req_post.get('gender_1')
    return HttpResponse('asdasdsad')