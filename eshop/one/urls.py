from django.urls import path, include, re_path
from . import views

urlpatterns = [
    path('', views.index, name='post_list'),
    path('category /<slug:slug>', views.category_items, name='category'),
    path('test-form/', views.test_form),
    # re_path(r'^(?P<slug>[\w-]+)/$', views.post_detail, name="detail"),
    # path('', views.index),
    # re_path(r'^category/(?P<hierarchy>.+)/$', views.index, name='category'),
    # re_path(r'^category/(?P<hierarchy>.+)/$', views.show_category, name='category'),
]
