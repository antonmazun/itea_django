from django.contrib import admin
from .models import *


# Register your models here.


class CategoriesAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Category, CategoriesAdmin)
admin.site.register(Laptop)
admin.site.register(Example)
admin.site.register(Author)
admin.site.register(Book)
admin.site.register(Place)
admin.site.register(Restaurant)
admin.site.register(Waiter)
admin.site.register(Publication)
admin.site.register(Article)
