from django.shortcuts import render, redirect
from django.http import HttpResponse
from one.views import global_ctx
from one.models import Restaurant
from .forms import ResraurantForm
from .models import Resraurant


# Create your views here.

def priv_list(request):
    ctx = {}
    if request.user.is_authenticated:
        ctx['len'] = len(Resraurant.objects.all())
        global_ctx.update(ctx)
        return render(request, 'action_user/priviligies.html', global_ctx)


def create_restaurant(request):
    ctx = {}
    form = ResraurantForm()
    ctx['form'] = form
    if request.method == "GET":
        global_ctx.update(ctx)
        return render(request,
                      'action_user/restaurant.html',
                      global_ctx)

    if request.method == 'POST':
        form = ResraurantForm(request.POST)
        if form.is_valid():
            # form_data = form.cleaned_data
            # creator = request.user
            # name = form_data['name']
            # address = form_data['address']
            # description = form_data['description']
            try:
                rest = form.save(commit=False)
                rest.creator = request.user
                rest.save()
                # Resraurant.objects.create(
                #     creator=creator,
                #     name=name,
                #     address=address,
                #     description=description,
                # )
                return redirect('action_user:my-actions')
            except Exception as e:
                ctx['error'] = e
                global_ctx.update(ctx)
                return render(request, 'action_user/restaurant.html', global_ctx)


def edit_restaurant(request, pk):
    ctx = {}
    restaurant_obj = Resraurant.objects.get(pk=pk)
    if request.method == 'GET':
        form = ResraurantForm(instance=restaurant_obj)
        ctx['form'] = form
        global_ctx.update(ctx)
        return render(request, 'action_user/edit_rest.html', global_ctx)
    elif request.method == 'POST':
        form = ResraurantForm(request.POST, instance=restaurant_obj)
        if form.is_valid():
            rest = form.save(commit=False)
            rest.creator = request.user
            rest.save()
            return redirect('action_user:my-actions')


def show_restaurant(request, pk):
    ctx = {}
    rest_object = Resraurant.objects.get(pk=pk)
    ctx['object_rest'] = rest_object
    global_ctx.update(ctx)
    return render(request , 'action_user/show_restaurant.html' ,global_ctx )



def delete_restaurant(request , pk):
    ctx = {}
    Resraurant.objects.get(pk=pk).delete()
    global_ctx.update(ctx)
    return redirect('action_user:show-all-restaurants')

def show_all_rest(request):
    ctx = {}
    all_rst = Resraurant.objects.all()
    ctx['all_rst'] = all_rst
    global_ctx.update(ctx)
    return render(request, 'action_user/all_rest.html', global_ctx)
