from django.urls import path, include, re_path
from . import views

app_name = 'action_user'
urlpatterns = [
    path('',
         views.priv_list,
         name='my-actions'),
    path('create-restaurant',
         views.create_restaurant,
         name='create-restaurant'),
    path('show-all-restaurants',
         views.show_all_rest,
         name='show-all-restaurants'),
    path('edit-restaurant/<int:pk>',
         views.edit_restaurant,
         name='edit-restaurant'),

    path('show-restaurant/<int:pk>',
         views.show_restaurant,
         name='show-instance'),

    path('delete-restaurant/<int:pk>',
         views.delete_restaurant,
         name='delete'),
]
