from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Resraurant(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name='название')
    address = models.CharField(max_length=255, verbose_name='Адрес')
    description = models.TextField(max_length=1500, verbose_name='Описание')

    def __str__(self):
        return self.name


class Photo(models.Model):
    image = models.ImageField(upload_to='images')
    title = models.CharField(max_length=255)
