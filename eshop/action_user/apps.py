from django.apps import AppConfig


class ActionUserConfig(AppConfig):
    name = 'action_user'
