from django.db import models
from ckeditor.fields import RichTextField


# Create your models here.
class Phone(models.Model):
    BRAND_CHOICE = (
        ('apple', 'Apple'),
        ('samsung', 'Samsung'),
        ('nokia', 'Nokia'),
    )
    title = models.CharField(max_length=255, verbose_name='Название')
    brand = models.CharField(max_length=255, verbose_name='Бренд', choices=BRAND_CHOICE)
    model = models.CharField(max_length=255, verbose_name='Модель')
    content = RichTextField()
    price = models.FloatField(verbose_name='Цена')
    sale = models.BooleanField(default=False, verbose_name="Товар по акции?")
    sale_percent = models.PositiveIntegerField(verbose_name='На сколько процентов снизить цену?', blank=True, null=True)
    create_at = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='images', verbose_name='Фото', default='', blank=True, null=True)

    def __str__(self):
        return self.title


    def to_dict(self):
        return {
            'id_': self.id,
            'title': self.title,
            'brand': self.brand,
            'price': self.price,
            'model': self.model,
            'sale': self.sale,
            'sale_percent': self.sale_percent,
        }
