from django.shortcuts import render , redirect
from django.http import HttpResponse
import datetime
from .models import Phone


# Create your views here.


def filters_index(request):
    text = """lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium ad amet commodi corporis cumque delectus,
    dolor doloremque dolores doloribus eius eligendi enim ex id impedit in incidunt iste itaque laboriosam laudantium
    modi nam obcaecati omnis, pariatur, quae quibusdam quidem quod reprehenderit sint sit sunt tempora veritatis
    voluptate? Alias amet aut commodi consequuntur cumque cupiditate dolores ducimus, ea excepturi in ipsum itaque iusto
    laborum minus mollitia nam nihil placeat, quas quis quo quos rem repellendus sunt unde velit voluptate voluptatibus!
    Alias consequatur doloremque esse, eveniet facere illum labore quasi, quisquam ratione reprehenderit rerum soluta
    temporibus veniam? Consequuntur tempore unde voluptate."""
    ctx = {
        'desc': text,
        'date_var': datetime.datetime.now(),
        'list': [],
        'a': 0,
        'dict_data': {
            'people': {
                'name': 'test',
                'surname': 'test1',
                'age': 10,
            }
        }
    }
    return render(request, 'filters/filter_index.html', ctx)


def show_phones(request):
    ctx = {

    }
    all_phones = Phone.objects.all()
    ctx['all_phones'] = all_phones
    return render(request, 'filters/all_phones.html', ctx)


def show_detail(request, pk):
    ctx = {
        'object': Phone.objects.get(id=pk)
    }
    return render(request, 'filters/phone_object.html', ctx)


def add_backet(request, pk):
    phone = Phone.objects.get(id=pk)
    # backet = {
    #     'user_id': request.user.id if request.user.id else 'anonym',
    #     'backet_list': []
    # }
    # request.session['backet'] = backet
    # request.session['backet']['backet_list'].append(phone.to_dict())

    backet_list = request.session['backet']['backet_list']
    backet_list.append(phone.to_dict())
    request.session.modified = True
    return render(request, 'filters/all_phones.html', {
        'ok': True
    })


def show_backet(request):
    if request.session.get('backet'):
        backet_list = request.session.get('backet').get('backet_list')
        count_sum = 0
        sales_list = []
        for elem in backet_list:
            count_sum += elem['price']
            if elem['sale'] == True:
                sale = elem['price'], elem['sale_percent']
                sales_list.append(sale)
        sum_sales = count_sales(sales_list)
        print('asdasd', sum_sales)
        ctx = {
            'count_sum': count_sum,
            'sum_product_with_sales': sum_sales[0],
            'sum_sales': sum_sales[1],
        }
        return render(request, 'filters/backet.html', ctx)
    else:
        return render(request, 'filters/backet.html', {})


def count_sales(args):
    sum_product_with_sales = 0
    sum_sales = 0
    for elem in args:
        price, sale_per = elem[0], elem[1]
        sum_product_with_sales += price / 100 * (100 - sale_per)
        sum_sales += price / 100 * sale_per
    return sum_product_with_sales, sum_sales


def delete_product(request, pk):
    backet_list = request.session.get('backet').get('backet_list')
    copy_backet = backet_list.copy()
    print(copy_backet)
    for index, list_item in enumerate(copy_backet):
        try:
            if int(list_item['id_']) == int(pk):
                copy_backet.remove(list_item)
                break
        except Exception as e:
            print(e)
    request.session['backet']['backet_list'] = copy_backet
    return render(request, 'filters/backet.html', {})


def clear_backet(request):
    request.session.clear()
    if not request.session.get('backet'):
        backet = {
            'user_id': request.user.id if request.user.id else 'anonym',
            'backet_list': []
        }
        request.session['backet'] = backet
    return render(request, 'filters/backet.html', {})
