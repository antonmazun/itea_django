from django import template

register = template.Library()


@register.filter
def custom_upper(value):
    return list(value)[2].upper()


@register.filter
def add_class(value):
    if value % 3 == 0:
        return 'three'
    elif value % 3 == 1:
        return 'two'
    elif value % 3 == 2:
        return 'one'

@register.filter
def check_sale(value , arg):
    if not arg:
        if value:
            return 'sale-product'
        else:
            return ''
    elif arg == 'price':
        return 'sale-price'


@register.filter
def new_price(old_price , percent):
    return old_price * ((100 - percent)/100)