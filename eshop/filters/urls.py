from django.urls import path
from . import views


app_name = 'filters'
urlpatterns = [
    path('' , views.filters_index , name='filters_index'),
    path('show-phones' , views.show_phones , name='show-phones'),
    path('show-detail/<int:pk>' , views.show_detail , name='show-detail'),
    path('add-backet/<int:pk>' , views.add_backet , name='add-backet'),
    path('show_backet' , views.show_backet , name='show_backet'),
    path('delete_product/<int:pk>' , views.delete_product , name='delete_product'),
    path('clear-backet' , views.clear_backet , name='clear-backet'),
]
